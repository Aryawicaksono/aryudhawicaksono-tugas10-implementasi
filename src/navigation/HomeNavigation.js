import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../screen/Home';

const Stack = createStackNavigator();

export default function HomeNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Home" component={Home} />
    </Stack.Navigator>
  );
}
