import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {TextRegular} from './Text';

export const GlobalImage = ({source, size = 45}) => {
  return (
    <Image
      source={source}
      style={[
        styles.images,
        {
          height: size,
          width: size,
        },
      ]}
    />
  );
};
export const ChoiceImage = ({source, size = 45, text}) => {
  return (
    <View style={styles.container}>
      <Image
        source={source}
        style={[styles.images, {width: size, height: size}]}
      />
      <View style={{height: 9}} />
      <TextRegular size={9} color="#BB2427" text={text} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 95,
    width: 95,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFF',
  },
  images: {borderRadius: 5, resizeMode: 'contain'},
});
