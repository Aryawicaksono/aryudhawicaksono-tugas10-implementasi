import React from 'react';
import {View, TouchableOpacity, TextInput, StyleSheet} from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';

export const InputText = ({
  placeholderText = '',
  placeholderTextColor = 'grey',
  multiline = false,
  numberOfLines = 1,
  onChangeText,
  value,
  style,
  keyboardType = 'default',
}) => {
  return (
    <View style={[styles.container, style]}>
      <TouchableOpacity
        style={{
          width: '10%',
          justifyContent: 'flex-start',
          paddingVertical: 10,
          marginLeft: 15,
        }}>
        <Icon name="search1" size={17} color="#000" />
      </TouchableOpacity>
      <TextInput
        placeholder={placeholderText}
        placeholderTextColor={placeholderTextColor}
        style={{
          width: '90%',
          paddingHorizontal: 10,
        }}
        multiline={multiline}
        numberOfLines={numberOfLines}
        onChangeText={text => onChangeText(text)}
        value={value}
        keyboardType={keyboardType}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#F6F8FF',
  },
});
