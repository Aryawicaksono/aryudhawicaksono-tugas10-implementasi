import React from 'react';
import {ComplexButton} from './Button';
import {TouchableOpacity, View} from 'react-native';
import {TextRegular, TextThin} from './Text';

export default function Footer(onPressLove) {
  return (
    <View>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <TextRegular size={12} text="Rekomendasi terdekat" />
        <TouchableOpacity>
          <TextThin size={10} color="#E64C3C" text="View all" />
        </TouchableOpacity>
      </View>
      <View style={{height: 25}} />
      <ComplexButton
        source={require('../../asset/Gejayan.png')}
        rating="4.8 ratings"
        nama="Jack Repair Gejayan"
        alamat="Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . ."
        openClose="CLOSE"
        onPressLove={onPressLove}
      />
      <View style={{height: 8}} />
      <ComplexButton
        source={require('../../asset/Seturan.png')}
        rating="4.7 ratings"
        nama="Jack Repair Seturan"
        alamat="Jl. Seturan Kec. Laweyan . . ."
        openClose="OPEN"
      />
    </View>
  );
}
