import React from 'react';
import {Text, StyleSheet} from 'react-native';

export const TextRegular = ({
  size = 15,
  text,
  color,
  numberOfLines = 1,
  style,
}) => {
  return (
    <Text
      style={[styles.regularText, {fontSize: size, color: color}, style]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};
export const TextBold = ({
  size = 20,
  text,
  color,
  style,
  numberOfLines = 1,
}) => {
  return (
    <Text
      style={[styles.boldText, {fontSize: size, color: color}, style]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

export const TextThin = ({
  size = 10,
  text,
  color,
  style,
  numberOfLines = 1,
}) => {
  return (
    <Text
      style={[styles.boldText, {fontSize: size, color: color}, style]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

const styles = StyleSheet.create({
  regularText: {
    color: '#201F26',
    fontWeight: '600',
  },
  boldText: {
    color: '#0A0827',
    fontWeight: '700',
  },
  thinText: {
    fontWeight: '500',
    color: '#D8D8D8',
  },
});
