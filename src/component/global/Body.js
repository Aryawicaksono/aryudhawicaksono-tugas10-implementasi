import React from 'react';
import {ImageButton} from './Button';
import {View} from 'react-native';

export default function Body() {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 17,
        marginBottom: 27,
      }}>
      <ImageButton source={require('../../asset/Sepatu.png')} text="Sepatu" />
      <ImageButton source={require('../../asset/Jaket.png')} text="Jaket" />
      <ImageButton source={require('../../asset/Ransel.png')} text="tas" />
    </View>
  );
}
