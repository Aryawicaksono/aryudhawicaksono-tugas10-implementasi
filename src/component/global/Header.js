import React from 'react';
import {View, TouchableOpacity} from 'react-native';

import {TextRegular} from './Text';
import {GlobalImage} from './Images';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icons from 'react-native-vector-icons/FontAwesome5';
import {InputText} from './InputText';

export default function Header({onChangeText}) {
  return (
    <View
      style={{
        backgroundColor: '#FFF',
        paddingBottom: 20,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 10,
        }}>
        <View>
          <GlobalImage source={require('../../asset/User.png')} />
          <TextRegular color="#034262" text="Hello, Agil" numberOfLines={1} />
        </View>
        <TouchableOpacity>
          <Icons name="shopping-bag" size={24} color="#000" />
        </TouchableOpacity>
      </View>
      <View
        style={{
          marginTop: 20,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <InputText onChangeText={onChangeText} style={{width: '80%'}} />
        <TouchableOpacity
          style={{
            height: 45,
            width: 45,
            borderRadius: 10,
            backgroundColor: '#F6F8FF',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Icon name="sliders" size={20} color="#000" />
        </TouchableOpacity>
      </View>
    </View>
  );
}
