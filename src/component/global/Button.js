import {ChoiceImage} from './Images';
import React from 'react';
import {TouchableOpacity, Image, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icons from 'react-native-vector-icons/AntDesign';
import {TextBold, TextRegular, TextThin} from './Text';

export const ImageButton = ({source, text}) => {
  return (
    <TouchableOpacity>
      <ChoiceImage source={source} text={text} />
    </TouchableOpacity>
  );
};

export const ComplexButton = ({
  source,
  nama,
  alamat,
  rating,
  openClose,
  onPressLove,
}) => {
  return (
    <TouchableOpacity
      style={{
        paddingVertical: 2,
        paddingHorizontal: 6,
        borderRadius: 10,
        backgroundColor: '#FFF',
        flexDirection: 'row',
      }}>
      <Image
        source={source}
        style={{width: 80, height: 121, resizeMode: 'contain'}}
      />
      <View style={{marginTop: 10, marginLeft: 10, width: '75%'}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: 51,
              marginBottom: 5,
            }}>
            <Icon name="star" size={8} color="#FFC107" />
            <Icon name="star" size={8} color="#FFC107" />
            <Icon name="star" size={8} color="#FFC107" />
            <Icon name="star" size={8} color="#FFC107" />
            <Icon name="star" size={8} color="#FFC107" />
          </View>
          <TouchableOpacity onPress={onPressLove}>
            <Icons name="heart" size={13} color="#F94343" />
            {/* {setLove === true ? (
              <Icons name="heart" size={13} color="#F94343" />
            ) : (
              <Icons name="hearto" size={13} color="#F94343" />
            )} */}
          </TouchableOpacity>
        </View>
        <TextThin size={10} color="#D8D8D8" text={rating} />
        <TextBold text={nama} color="#201F26" size={12} />
        <TextThin size={9} text={alamat} color="#D8D8D8" />
        <View
          style={{
            marginTop: 25,
            height: 21,
            width: 58,
            borderRadius: 30,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: openClose === 'CLOSE' ? '#E64C3C33' : '#11A84E1F',
          }}>
          <TextBold
            size={12}
            color={openClose === 'CLOSE' ? '#EA3D3D' : '#11A84E'}
            text={openClose}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};
