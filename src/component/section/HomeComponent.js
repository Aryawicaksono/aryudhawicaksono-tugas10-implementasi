import React from 'react';
import {ScrollView} from 'react-native';
import Header from '../global/Header';
import Body from '../global/Body';
import Footer from '../global/Footer';

export default function HomeComponent(onChangeText, onPressLove) {
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Header onChangeText={onChangeText} />
      <Body />
      <Footer onPressLove={onPressLove} />
    </ScrollView>
  );
}
