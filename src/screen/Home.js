import React, {useState} from 'react';
import HomeComponent from '../component/section/HomeComponent';
import {View} from 'react-native';

export default function Home() {
  const [love, setLove] = useState(false);
  const Love = () => setLove(true);

  return (
    <View
      style={{
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 15,
        paddingTop: 10,
        flex: 1,
      }}>
      <HomeComponent />
    </View>
  );
}
